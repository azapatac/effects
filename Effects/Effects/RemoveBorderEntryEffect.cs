﻿namespace Effects.Effects
{
    using Xamarin.Forms;

    public class RemoveBorderEntryEffect : RoutingEffect
    {
        public RemoveBorderEntryEffect() : base($"University.{nameof(RemoveBorderEntryEffect)}") { }
    }
}